import axios from "axios";
import store from "../store";
import { LOGOUT } from "../actions/types";

const api = axios.create({
  baseURL: "/api",
  headers: {
    "Content-Type": "application/json",
  },
});
/**
 intercepter les réponses d'erreur de l'API
 et vérifiez si le jeton n'est plus valide.
 c'est à dire. Le jeton a expiré ou l'utilisateur n'est plus
 authentifié.
 déconnecter l'utilisateur si le jeton a expiré
**/

api.interceptors.response.use(
  (res) => res,
  (err) => {
    if (err.response.status === 401) {
      store.dispatch({ type: LOGOUT });
    }
    return Promise.reject(err);
  },
);

export default api;
