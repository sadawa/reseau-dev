import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import rootReducer from "./reducers";
import setAuthToken from "./utils/setAuthToken";

const initialState = {};

const middleware = [thunk];

const store = createStore(
  rootReducer,
  initialState,
  composeWithDevTools(applyMiddleware(...middleware)),
);
//configurer un écouteur d'abonnement au magasin
// pour stocker le jeton des utilisateurs dans localStorage

// initialise l'état actuel à partir du magasin redux pour la comparaison des abonnements
// évite une erreur indéfinie
let currentState = store.getState();

store.subscribe(() => {
  // garder une trace de l'état précédent et actuel pour comparer les changements
  let previousState = currentState;
  currentState = store.getState();
  // si le jeton change, définissez la valeur dans les en-têtes localStorage et axios
  if (previousState.auth.token !== currentState.auth.token) {
    const token = currentState.auth.token;
    setAuthToken(token);
  }
});

export default store;
