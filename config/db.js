const mongoose = require("mongoose");
const config = require("config");
const db = config.get("mongoURI");

const connectDB = async () => {
  //on utilise un try catch pour essaie de voir si on est bien connecte a la base de donnée
  try {
    await mongoose.connect(db, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });

    console.log("MongoDB connected...");
  } catch (err) {
    console.error(err.message);
    // sortie du process si sa fail
    process.exit(1);
  }
  mongoose.set("useFindAndModify", false);
};

module.exports = connectDB;
